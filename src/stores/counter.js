import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useCounterStore = defineStore('counter', () => {
  const globalCount = ref(0)
  const listItems = ref([[], []])

  function incrementGlobally() {
    globalCount.value++
  }

  function addItemToList(listIndex) {
    incrementGlobally()

    const itemObj = {
      id: Math.random().toString(36).substring(2, 6),
      name: `Item #${globalCount.value}`
    }

    listItems.value[listIndex].push(itemObj)
  }

  // COMMENT: unclear if I should remove one from the counter here
  function removeItemFromList(listIndex, itemId) {
    listItems.value[listIndex] = listItems.value[listIndex].filter((item) => item.id !== itemId)
  }

  return { globalCount, listItems, addItemToList, removeItemFromList }
})
